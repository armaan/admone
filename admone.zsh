# Admone Zsh prompt
# Copyright Armaan Bhojwani 2021 <me@armaanb.net>
# MIT licensed, see the LICENSE file for more information.

PROMPT_CHAR="➤"

zstyle ':vcs_info:git:*' formats '%f|%F{green}%b%u%c'
zstyle ':vcs_info:git:*' actionformats '%b|%a%u%c'
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'

################################################################################

# Cursor shape depends on vi mode
function zle-keymap-select zle-line-init zle-line-finish {
  case $KEYMAP in
    vicmd)
      # Block
      print -n -- "\E]50;CursorShape=0\C-G"
      ;;
    viins|main)
      # Beam
      print -n -- "\E]50;CursorShape=1\C-G"
      ;;
  esac
  # zle reset-prompt
  zle -R
}

# Set everything!
set-prompt() {
  PROMPT="%B%F{%(?.cyan.red)}${PROMPT_CHAR}%f%b "
  RPROMPT="[%F{blue}%~${vcs_info_msg_0_}%f][%(?,%F{cyan}%?,%F{red}%?)%f]"

  [[ -v PS1_NL ]] && echo || PS1_NL=
}

zstyle ':vcs_info:*' check-for-changes true
autoload -Uz add-zsh-hook vcs-info
add-zsh-hook precmd vcs_info
add-zsh-hook precmd set-prompt

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select
